Feature: Api web services-Location

    Location

    @Generacion tokens
  Scenario:  Generar clientId y clientSecret para acceso exitoso
    Given Se inicia sesion en la pagina para crear el evento 
    When Se ingresa el nombre de la nueva conexion y se crea
    Then Se genera el clientId y el clientSecret.

@Generacion 
  Scenario:  No Generar conexion ya existente
    Given Se inicia sesion en la pagina 
    When Se ingresa el nombre de una conexion ya existente
    Then No se genera la nueva conexion 
 
@NoGeneracion 
  Scenario:  No crear conexion por no ingresar nombre
    Given Se inicia sesion en la pagina para crearla
    When No se ingresa el nombre y se da en crear
    Then No se crea la nueva conexion   


    Scenario:  Crear query de todos los eventos para las ultimas placas ingresadas.
        Given Inicio la búsqueda  de todas placas ingresadas en el ultimo tiempo
       
    Scenario:  Generar query de todos los eventos para las ultimas placas ingresadas con atributos.
        Given Inicio la búsqueda de los atributos para todas las placas ingresadas en el ultimo tiempo

    Scenario:  Crear query de una placa especifica ingresada en el ultimo tiempo.
        Given Inicio la búsqueda  ingresando una  placa del automovil
        Then El sistema obtiene como resultado la informacion de la placa ingresada

  Scenario:  Generar query de todos los eventos de las ultimas placas por offset.
        Given Inicio la búsqueda  de todas las ultimas placas ingresadas en el offset

    Scenario:  Originar query de los eventos para una placa en especifico ingresada por offset.
        Given Inicio la búsqueda  de placa  en el ultimo offset

    Scenario:  Generar query de todos los eventos de las ultimas placas por offset con atributos.
        Given Inicio la búsqueda  de los atributos por  placa  en el ultimo offset
    
    Scenario:  Crear query de todos los eventos generados en las ultimas fechas.
        Given Inicio la búsqueda  de todas las ultimas placas ingresadas la ultima fecha

    Scenario:  Generar query de todos los eventos en las ultimas fechas con sus atributos.
        Given Inicio la búsqueda de los atributos de todas las ultimas placas ingresadas la ultima fecha
    
    Scenario:  Realizar query de fecha por placa.
        Given Inicio la búsqueda  de fecha por placa

    Scenario:  Generar query de fecha con placa equivocada.
        Given Ingreso una placa que no está en la base de datos
        Then Arroja un resultado no exitoso