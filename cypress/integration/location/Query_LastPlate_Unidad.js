import { Given, Then } from "cypress-cucumber-preprocessor/steps";
import { clientd, secretd} from "./Clients_user_inexits"

var token1 = ""

   
    



const query = `{
    

        last(plate: "644NCE") {
    
        serviceCode, speed, latitude, longitude, addressTown, address, generationDate, generationDateGMT, hour, near, arrivalTimeStamp, ignition, heading, minStopped, levelBattery, temperature, metersCustomerPoint, nameCustomerPoint, description, state, codeEventUnified, deviceType, locationStatus, name
    
        }
    
    
}`;

    Given(`Inicio la búsqueda  ingresando una  placa del automovil`, () => {
                
        cy.request({
            method: 'POST',
            url:'http://40.70.70.158:8080/auth/realms/satrack-base/protocol/openid-connect/token',
            form: true,
            body: {
                grant_type:"client_credentials",
                client_id: clientd,
                client_secret: secretd
            }
        }).then((response) => {
           
            token1 = response.body.access_token
        })
        cy.wait(3600)
        cy.request({
                    method: 'POST',
                    url:'http://52.167.139.83/api/event',
                    auth: {
                        bearer: token1
                    },
                    body: {query},
                    failOnStatusCode: false
               
                })

    });

  Then(`El sistema obtiene como resultado la informacion de la placa ingresada`, () => {
    cy.request({
        method: 'POST',
        url:'http://52.167.139.83/api/event',
        failOnStatusCode: false,
        auth: {
        bearer: token1
        },
        body: {query}
    }).then((response) => {
        
        expect(response.status).to.eq(200)
        expect(response.body).to.have.property('last').property(0).to.have.property('serviceCode',"644NCE")
    })
    cy.wait(3600)
    });