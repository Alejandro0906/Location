import { Given, Then } from "cypress-cucumber-preprocessor/steps";
import { clientd, secretd} from "./Clients_user_inexits"

var token7 = ""

    
    
    


const query = `{
    byDate(plate: "PRUEBASHC3", initialDate: "06/11/2019 05:05:54", finalDate: "06/11/2021 05:05:59", currentPage: 1, itemsPerPage: 10) {
        pagination {
    
          currentPage
    
        }

        events {
    
          serviceCode, speed, latitude, longitude, ignitionState, heading, gpsDate, gpsFix, systemDate, address, state, city, neighborhood, odometer, eventCode, unifiedCode
    
        }
    
      }
    
    
    
}`;
    Given(`Inicio la búsqueda  de fecha por placa`, () => {

        cy.request({
            method: 'POST',
            url:'http://40.70.70.158:8080/auth/realms/satrack-base/protocol/openid-connect/token',
            form: true,
            failOnStatusCode: false,
            body: {
                grant_type:"client_credentials",
                client_id: clientd,
                client_secret: secretd
            }
            
        }).then((response) => {
           
            token7 = response.body.access_token
        })
        cy.wait(3600)

                cy.request({
                    method: 'POST',
                    url:'http://52.167.139.83/api/event',
                    auth: {
                        bearer: token7
                    },
                    body: {query},
                    failOnStatusCode: false,
                   
                }).then((response) => {
                    expect(response.status).to.eq(200)
                    expect(response.body).to.have.property('byDate').property('events').property(0).to.have.property('serviceCode',"PRUEBASHC3")

                })
                token7=""
                cy.wait(3600)
    });
