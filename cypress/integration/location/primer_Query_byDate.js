import { Given, Then } from "cypress-cucumber-preprocessor/steps";
import { clientd, secretd} from "./Clients_user_inexits"



var token6 = ""

    
    


// 1er query 
const query = `{
    
    byDate(plate: "", initialDate: "06/11/2019 05:05:54", finalDate: "06/11/2021 05:05:59", currentPage: 1, itemsPerPage: 10) {
        pagination {
          currentPage
        }
        events {
          serviceCode, speed, latitude, longitude, ignitionState, heading, gpsDate, gpsFix, systemDate, address, state, city, neighborhood, odometer, eventCode, unifiedCode
        }
      }
    
    
    
}`;

    Given(`Inicio la búsqueda  de todas las ultimas placas ingresadas la ultima fecha`, () => {
    
        cy.request({
            method: 'POST',
            url:'http://40.70.70.158:8080/auth/realms/satrack-base/protocol/openid-connect/token',
            form: true,
            failOnStatusCode: false,
            body: {
                grant_type:"client_credentials",
                client_id: clientd,
                client_secret: secretd
            } 
            
        }).then((response) => {
           
            token6 = response.body.access_token
           
        })
                cy.request({
                    method: 'POST',
                    url:'http://52.167.139.83/api/event',
                    auth: {
                        bearer: token6
                    },
                    body: {query},
                    failOnStatusCode: false,
                   
               
                }).then((response) => {
                    
                    expect(response.status).to.eq(200)
                    expect(response.body).to.have.property('byDate').property('events').property(0).to.have.property('serviceCode',"PRUEBASHC3")
                    expect(response.body).to.have.property('byDate').property('events').property(5).to.have.property('serviceCode',"CALAMP8")
  
                   
                })
                cy.wait(3600)
                token6=""
    });
