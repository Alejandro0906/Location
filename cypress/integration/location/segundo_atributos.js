import { Given, Then } from "cypress-cucumber-preprocessor/steps";
import { clientd, secretd} from "./Clients_user_inexits"

var token2 = ""

    


const query = `{
    

    last(plate: "") {

    serviceCode, speed, latitude, longitude, addressTown, address, generationDate, generationDateGMT, hour, near, arrivalTimeStamp, ignition, heading, minStopped, levelBattery, temperature, metersCustomerPoint, nameCustomerPoint, description, state, codeEventUnified, deviceType, locationStatus, name

    }


}`;

    Given(`Inicio la búsqueda de los atributos para todas las placas ingresadas en el ultimo tiempo`, () => {
    
        cy.request({
            method: 'POST',
            url:'http://40.70.70.158:8080/auth/realms/satrack-base/protocol/openid-connect/token',
            form: true,
            failOnStatusCode: false,
            body: {
                grant_type:"client_credentials",
                client_id: clientd,
                client_secret: secretd
            }
            
        }).then((response) => {
           
            token2 = response.body.access_token
           
        })
                cy.request({
                    method: 'POST',
                    url:'http://52.167.139.83/api/event',
                    auth: {
                        bearer: token2
                    },
                    body: {query},
                    failOnStatusCode: false,
                   
               
                }).then((response) => {
                    
                    expect(response.status).to.eq(200)

                    expect(response.body).to.have.property('last').property(0).to.have.property('serviceCode',"644NCE")
                    expect(response.body).to.have.property('last').property(0).to.have.property('deviceType' ,'CELLO-IQ30')

                    expect(response.body).to.have.property('last').property(0).to.have.property('addressTown','Bogotá D.C., Distrito Capital')

                    



                   
                })
                token2=""
                cy.wait(3600)
    });
