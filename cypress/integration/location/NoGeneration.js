import { Given, Then } from "cypress-cucumber-preprocessor/steps";

import {Page} from "../../pageObjects/allObjects"


var token = "" 

const todoPage = new Page()


Cypress.Commands.add('getIframeBody', () => {
       
    return cy.get('iframe[id="externalContentFrame"]')
    
    .its('0.contentDocument.body').should('not.be.empty')
    .then(cy.wrap)

  })

    
Given(`Se inicia sesion en la pagina para crearla`, () => {      
    todoPage.navigate()
    todoPage.login()
    
});

  
When(`No se ingresa el nombre y se da en crear`, () => {  
  
    todoPage.sinName()
});

Then(`No se crea la nueva conexion`, () => {  

    cy.getIframeBody().contains('El campo nombre es obligatorio').should('exist') 

});
