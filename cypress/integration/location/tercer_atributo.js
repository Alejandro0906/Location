import { Given, Then } from "cypress-cucumber-preprocessor/steps";
import { clientd, secretd} from "./Clients_user_inexits"

var token2 = ""

    

// 3er query 
const query = `{
    
         byOffset(plate: "", consumerId: "consumer3") {
  
              serviceCode, speed, latitude, longitude, ignitionState, heading, gpsDate, gpsFix, systemDate, address, state, city, neighborhood, odometer, eventCode, unifiedCode  
            }
    
}`;

    Given(`Inicio la búsqueda  de los atributos por  placa  en el ultimo offset`, () => {
        cy.request({
            method: 'POST',
            url:'http://40.70.70.158:8080/auth/realms/satrack-base/protocol/openid-connect/token',
            form: true,
            failOnStatusCode: false,
            body: {
                grant_type:"client_credentials",
                client_id: clientd,
                client_secret: secretd
            }  
        }).then((response) => {
           
            token2 = response.body.access_token
        })        
        cy.request({
                    method: 'POST',
                    url:'http://52.167.139.83/api/event',
                    auth: {
                        bearer: token2
                    },
                    body: {query},
                    failOnStatusCode: false,

                }).then((response) => {
                    expect(response.status).to.eq(200)
                    expect(response.body).to.have.property('byOffset').to.have.property('length').to.eq(0)
                })
                token2=""
                cy.wait(3600)
    });
