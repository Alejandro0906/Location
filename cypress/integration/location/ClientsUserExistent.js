import { Given, Then } from "cypress-cucumber-preprocessor/steps";

import {Page} from "../../pageObjects/allObjects"

const todoPage = new Page()
var token = "" 
beforeEach(() => {
    cy.request({
        method: 'POST',
        url:'http://40.70.70.158:8080/auth/realms/satrack-base/protocol/openid-connect/token',
        form: true,
        failOnStatusCode: false,
        body: {
            grant_type:"client_credentials",
            client_id: "deliverymgmt-web",
            client_secret: "c52cf1c8-381d-4baa-be0b-3dfaa8defebe"
        }
    }).then((response) => {
        
        token = response.body.access_token 
    })
});

Cypress.Commands.add('getIframeBody', () => {
       
    return cy.get('iframe[id="externalContentFrame"]')
    
    .its('0.contentDocument.body').should('not.be.empty')
    .then(cy.wrap)

  })



  //http://login.satrack-test.net/
  
Given(`Se inicia sesion en la pagina`, () => {      
    todoPage.deleteClientExistent(token)
    todoPage.addClientAPI(token,"prueba-test")
    todoPage.navigate()
    todoPage.login()
    
   
    
});

  
When(`Se ingresa el nombre de una conexion ya existente`, () => {  

    todoPage.addClient("prueba-test")
});

Then(`No se genera la nueva conexion`, () => {  

   todoPage.addClientAPIError(token, "prueba-test")

});  