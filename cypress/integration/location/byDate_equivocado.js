import { Given, Then } from "cypress-cucumber-preprocessor/steps";
import { clientd, secretd} from "./Clients_user_inexits"

var token9 = ""

    


const query = `{
    byDate(plate: "PRUEBAHC4", initialDate: "06/11/2019 05:05:54", finalDate: "06/11/2021 05:05:59", currentPage: 1, itemsPerPage: 10) {
        pagination {
          currentPage
        }
        events {
          serviceCode, speed, latitude, longitude, ignitionState, heading, gpsDate, gpsFix, systemDate, address, state, city, neighborhood, odometer, eventCode, unifiedCode
        }
      }   
}`;



    Given(`Ingreso una placa que no está en la base de datos`, () => {
    
        cy.request({
            method: 'POST',
            url:'http://40.70.70.158:8080/auth/realms/satrack-base/protocol/openid-connect/token',
            form: true,
            failOnStatusCode: false,
            body: {
                grant_type:"client_credentials",
                client_id: clientd,
                client_secret: secretd
            }
        }).then((response) => {
            token9 = response.body.access_token
        })
    
        cy.request({
            method: 'POST',
            url:'http://52.167.139.83/api/event',
            auth: {
                bearer: token9
            },
            body: {query},
            failOnStatusCode: false,
           
       
        })
});




Then(`Arroja un resultado no exitoso`, () => {
cy.request({
    method: 'POST',
    url:'http://52.167.139.83/api/event',
    failOnStatusCode: false,
    auth: {
    bearer: token9
    },
    body: {query}


}).then((response) => {
  
    expect(response.status).to.eq(500)
   
})

token9=""
cy.wait(3600)
});

