const report = require('multiple-cucumber-html-reporter');
const date = new Date();
// const env = require('../../cypress.env');


report.generate({
    jsonDir: './cypress/cucumber-json/',
    reportPath: './compliance-report',
    pageTitle:  ' Reports Satrack',
    reportName: 'Compliance Report',
    displayDuration: true,
    useCDN: true,
    metadata:{
        browser: {
            name: 'chrome',
            version: '60'
        },
        device: 'Test machine',
        platform: {
            name: 'ubuntu',
            version: '18.04'
        }
    },
    customData: {
        title: 'Run info',
        data: [
            {label: 'Project', value: 'Reports Satrack'},
            {label: 'Release', value: '1.0.0'},
            {label: 'Execution Start Time', value: date.getFullYear()+'-'+( date.getMonth()+1)+'-'+ date.getDate()+ ' '+
            date.getHours() + ":" +  date.getMinutes() + ":" +  date.getSeconds()}
        ]
    }
});