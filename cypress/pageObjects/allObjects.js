export class Page {
    navigate() {
         //http://login.satrack-test.net/
         //http://104.209.248.57/#/customlogin
        cy.visit("http://104.209.248.57/")
    }
    login(){
        // cy.get('input[placeholder="Usuario"]').type("camilo.orrego")
        // cy.get('input[placeholder="Contraseña"]').type("camilo1234")
        // cy.get('.login-button').click()
   
        cy.get("#UserName").type("camilo.orrego")
        cy.get("#Password").type("camilo1234")
        cy.get('.button-app').click()
    }
    deleteClient(token){
        cy.request({
            method: 'DELETE',
            url:'http://40.79.44.53/api/keycloak?externalClient=external-client-test-ft&owner=camilo.orrego',
            auth: {
                bearer: token
            }
            
          })
    }
    deleteClientExistent(token_){
        cy.request({
            method: 'DELETE',
            url:'http://40.79.44.53/api/keycloak?externalClient=external-client-prueba-test&owner=camilo.orrego',
            auth: {
                bearer: token_
            }
            
          })
    }
    addClient(nameClient){
        cy.get('#iconDirective',{timeout: 20000}).click()
        cy.wait(10000)
        cy.contains('Seguridad').click()
        cy.wait(15000)
        cy.getIframeBody().find('#btnOpenDialog').click()
        cy.getIframeBody().find('#mat-input-0').type(nameClient)
        cy.getIframeBody().contains('Integrador').click()
        cy.getIframeBody().contains('CREAR').click()
        cy.wait(5000)
    }
    addClientAPIError(aut,client){
        cy.request({
            method: 'POST',
            url:'http://40.79.44.53/api/keycloak',
            failOnStatusCode: false,
            auth: {
                bearer: aut
            },
            body: {
                protocolMapper: {
                clientIdSuffix: client,
                userName: "camilo.orrego"
                },
                services: [
                    {
                    documentationUrl: "string",
                    partitionKey: "1",
                    publicName: "string",
                    rowKey: "3"
                    }
                ]
            }
        
        }).then((response) => {
            expect(response.status).to.eq(500)
        })
    }
    sinName(){
        cy.get('#iconDirective',{timeout: 20000}).click()
        cy.wait(10000)
        cy.contains('Seguridad').click()
        cy.wait(15000)
        cy.getIframeBody().find('#btnOpenDialog').click()
        cy.getIframeBody().contains('CREAR').click()

    }
    addClientAPI(aut,client){
        cy.request({
            method: 'POST',
            url:'http://40.79.44.53/api/keycloak',
            
            auth: {
                bearer: aut
            },
            body: {
                protocolMapper: {
                clientIdSuffix: client,
                userName: "camilo.orrego"
                },
                services: [
                    {
                    documentationUrl: "string",
                    partitionKey: "1",
                    publicName: "string",
                    rowKey: "3"
                    }
                ]
            }
        
        })
    }
}
